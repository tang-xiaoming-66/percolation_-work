import random
import numpy as np
import time
from scipy.ndimage import measurements
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
font_set = FontProperties(fname=r"/usr/share/fonts/truetype/wqy/wqy-zenhei.ttc", size=20)

class DSU:
    def __init__(self, n):
        self.count = n
        self.size = [1 for i in range(n)]
        self.parent = [i for i in range(n)]


    def Isconnected(self, p, q):
        self.check(p)
        self.check(q)
        return self.find(p) == self.find(q)

    def find(self, p):  # 找到点p所属团簇的根节点
        self.check(p)                  # 调用自定义函数  校验p的长度是否合理
        if (p == self.parent[p]):  # 当点p的父亲节点就是它本身时，p就为自身的根节点
            return p
        else:
            self.parent[p] = self.find(self.parent[p])  # 通过递归调用find（parent[p])来向上访问元素，并且把找到的根元素赋值给节点的父亲指针
            return self.parent[p]

    def check(self, p):  # 自定义一个参数校验函数
        n = len(self.size)  # len(列表变量的名称）：可以统计列表中元素的总数
        if (p < 0 or p >= n):
            raise Exception('超出范围!')

    def union(self, p, q):  # 判断p和 q的根节点是否相连，不相连就把他们的根节点连起来
        self.check(p)
        self.check(q)
        P_root = self.find(p)  # 找到节点p的根节点rootp
        Q_root = self.find(q)  # 找到节点q的根节点rootq
        if P_root == Q_root:  # 判断两个根节点是否相同，不相同则证明p q不相连
            return
        if self.size[P_root] < self.size[Q_root]:
            self.parent[P_root] = Q_root
            self.size[Q_root] = self.size[Q_root] + self.size[P_root]
        else:
            self.parent[Q_root] = P_root
            self.size[P_root] = self.size[P_root] + self.size[Q_root]



class PercolationModel:  # 渗流  建立一个Percolation类，其中有一个 N×N 的二维数组，将开格记为1，闭格记为0，默认初始值均为0（全是闭格）。
    def __init__(self, N):
        self.Array = [[0 for i in range(N)] for j in range(N)]  # 二维数组，创建一个N*N的矩阵，元素全为0
        self.size = N
        self.DSU1 = DSU(N * N + 2)
        self.aa1 = self.DSU1.parent

    # 如果格子尚未打开，则打开格子（第i行，第j列）
    def OpenSize(self, i, j):  # 检查坐标为(i, j)的点是否开启，如果没有开启，则将其开启  并考虑四周是否可以连接
        if self.Array[i][j] == 1:
            return
        else:
            self.Array[i][j] = 1

            if i != 0:
                if self.isOpen(i - 1, j):
                    self.DSU1.union(i * self.size + j + 1, (i - 1) * self.size + j + 1)
            if i != self.size - 1:
                if self.isOpen(i + 1, j):
                    self.DSU1.union(i * self.size + j + 1, (i + 1) * self.size + j + 1)
            if j != 0:
                if self.isOpen(i, j - 1):
                    self.DSU1.union(i * self.size + j + 1, i * self.size + j)
            if j != self.size - 1:
                if self.isOpen(i, j + 1):
                    self.DSU1.union(i * self.size + j + 1, i * self.size + j + 2)


            # for a in range(len(self.Array)):
            #     for b in range(len(self.Array[a])):
            #         print(self.Array[a][b],end='  ')
            #
            #     print()    # 打印一行后换行


    def isOpen(self, i, j):  # 判断（i,j）这个格子是否打开，如果打开（即等于1），则return
        return self.Array[i][j] == 1


#     def isFull(self, i, j):
#                               #检查该点是否已经打开并且已经与顶部的虚拟点（相当于顶部的一排）连通，用connected()方法检测；
#         return self.UF1.Isconnected(0, i * self.size + j + 1)

#     def percolates(self):       #检查顶部的虚拟点与底部的虚拟点是否连通，用connected()方法检测；
#         return self.UF.Isconnected(0, self.size * self.size + 1)   # 两个虚元素联通

class PercolationStats:  # 渗流状态统计   stats：统计    N：方阵大小  T：循环次数
    def __init__(self, N, T):  # 顺序的从列表中依次获取数据，每一次循环过程中，数据都会保存在i这个变量中，这个变量的

        self.Meanlist = []
        self.Groupnumlist = []
        self.Probabilitylist = []
        self.Maxlist = []
        self.Secondlist = []
        self.times = T
        for k in range(T):
            Test = PercolationModel(N)
            aq = Test.Array

            count = 0  # 定义整数变量用于记录循环次数（计数器）    # while循环：当条件满足时，在循环体内部不断的执行相同的操作。
            block_list = [i for i in range(N * N)]
            while block_list:  # 不用在每次一个点打开的时候，每次都循环一遍所有的点，加入到block_list中

                # print("单个%s"%block_list)

                to_open = random.choice(block_list)  # 从列表中随机取一个元素
                # print("随机打开%s"%to_open)
                # print("单个%s" % block_list)
                to_open_j = int(to_open % N)
                to_open_i = int((to_open - to_open_j) / N)
                Test.OpenSize(to_open_i, to_open_j)
                #                 if experiment.isOpen(to_open_i, to_open_j):  # 如果是关闭的
                block_list.remove(to_open)

                # aa = Percolation(N)
                # a = aa.Array
                # print(a)

                count = count + 1  # 处理计数器
                #                 lw, num = measurements.label(aq)
                array1 = Test.aa1
                x1 = max(array1, key=array1.count)  # 找到最大元素
                #                 print("最大%s"%x1)
                #                 print(array1)
                self.number1 = array1.count(x1)  # 最大元素的数量

                array2 = list(filter(lambda x: x != x1, array1))
                x2 = max(array2, key=array2.count)
                self.number2 = array2.count(x2)


                self.Maxlist.append(self.number1)
                self.Secondlist.append(self.number2)
                print("次数为：%s" % count)
                probability = count / (N * N)
                lw, num = measurements.label(aq)
                #                 # print(lw)
                # #                     print("此时连通分量为%s个" % num)
                meansize = count / num
                # #                     print("此时连通分量的平均大小为%s" % meansize)

                # #                     print("此时概率P为：", probability)
                self.Meanlist.append(meansize)
                self.Groupnumlist.append(num)
                self.Probabilitylist.append(probability)

            self.Num = np.array(self.Groupnumlist)

            self.Pro = np.array(self.Probabilitylist)

            self.Mean = np.array(self.Meanlist)

            self.MAX1 = np.array(self.Maxlist)
            self.Second1 = np.array(self.Secondlist)

            fig = plt.figure(figsize=(10, 5), dpi=100)
            fig.patch.set_facecolor('orange')
            fig.patch.set_alpha(0.6)
            plt.grid(alpha=0.8)

            plt1, = plt.plot(self.Pro, self.MAX1)
            plt2, = plt.plot(self.Pro, self.Second1)
            plt.legend(handles=[plt1, plt2], labels=['最大团簇', '次大团簇'], prop=font_set)
            plt.title(u"最大和次大连通分量随概率的变化折线图", fontsize=17, fontproperties=font_set)
            plt.xlabel("概率P", fontsize=17, fontproperties=font_set)  # X轴标题及字号
            plt.ylabel("连通分量大小", fontsize=17, fontproperties=font_set)

            plt.show()


#             plt.draw()
#             plt.savefig("./png",dpi=200)
            fig = plt.figure(figsize=(10, 5), dpi=100)
            fig.patch.set_facecolor('orange')
            fig.patch.set_alpha(0.6)
            plt.grid(alpha=0.8)

            plt.scatter(self.Pro,self.MAX1)
            plt.title(u"最大连通分量的数量随概率的变化散点图", fontsize=17,fontproperties=font_set)
            plt.xlabel("概率P", fontsize=17,fontproperties=font_set)#X轴标题及字号
            plt.ylabel("最大连通分量数目", fontsize=17,fontproperties=font_set)
            plt.show()

            fig = plt.figure(figsize=(10, 5), dpi=100)
            fig.patch.set_facecolor('orange')
            fig.patch.set_alpha(0.6)
            plt.grid(alpha=0.8)

            plt.plot(self.Pro,self.Num)
            plt.title(u"连通分量的数量随概率的变化折线图", fontsize=17,fontproperties=font_set)
            plt.xlabel("概率P", fontsize=17,fontproperties=font_set)#X轴标题及字号
            plt.ylabel("连通分量数目", fontsize=17,fontproperties=font_set)
#             plt.tick_params(axis='both', labelsize=10)
            plt.show()

            fig = plt.figure(figsize=(10, 5), dpi=100)
            fig.patch.set_facecolor('orange')
            fig.patch.set_alpha(0.6)
            plt.grid(alpha=0.8)

            plt.scatter(self.Pro,self.Num)
            plt.title(u"连通分量的数量随概率的变化散点图", fontsize=17,fontproperties=font_set)
            plt.xlabel("概率P", fontsize=17,fontproperties=font_set)#X轴标题及字号
            plt.ylabel("连通分量数目", fontsize=17,fontproperties=font_set)
            plt.show()

            fig = plt.figure(figsize=(10, 5), dpi=100)
            fig.patch.set_facecolor('orange')
            fig.patch.set_alpha(0.6)
            plt.grid(alpha=0.8)

            plt.plot(self.Pro,self.Mean)
            plt.title(u"连通分量的平均尺寸随概率的变化散点图", fontsize=17,fontproperties=font_set)
            plt.xlabel("概率P", fontsize=17,fontproperties=font_set)#X轴标题及字号
            plt.ylabel("连通分量平均尺寸", fontsize=17,fontproperties=font_set)
            plt.show()

            fig = plt.figure(figsize=(10, 5), dpi=100)
            fig.patch.set_facecolor('orange')
            fig.patch.set_alpha(0.6)
            plt.grid(alpha=0.8)

            plt.scatter(self.Pro,self.Mean)
            plt.title(u"连通分量的平均尺寸随概率的变化散点图", fontsize=17,fontproperties=font_set)
            plt.xlabel("概率P", fontsize=17,fontproperties=font_set)#X轴标题及字号
            plt.ylabel("连通分量平均尺寸", fontsize=17,fontproperties=font_set)
            plt.show()


if __name__ == '__main__':
    time_start = time.time()
    N = 30 # 请输入方阵大小和循环次数
    times = 1
    test = PercolationStats(N, times)
    time_end = time.time()
    print('总共花费时长为：', time_end - time_start)



